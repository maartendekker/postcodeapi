function getAdress() {
    var postcode = $('#postcode').val();
    var huisnummer = $('#huisnummer').val();
    console.log("Adresgegevens zoeken bij postcode: " + postcode + " en huisnummer: " + huisnummer);

    $.ajax({
        url: "https://postcode-api.apiwise.nl/v2/addresses/?postcode=" + postcode + "&number=" + huisnummer,
        dataType: "json",
        type: 'GET',
        headers: { // mitchell API key vxBUFxIV8XgAahAvTYn5abMbeL6Vss469ocOSRFd
                  // maarten API key ADkMO2Bbe46fjUGUH1t2B23bLR0qshKD79XtUfUY
            "X-Api-Key": "vxBUFxIV8XgAahAvTYn5abMbeL6Vss469ocOSRFd"
        },
        success: function(data) {
            for (i = 0; i < data._embedded.addresses.length; i++) { //vul de data in de invul vakjes
                document.getElementById('straat').value = data._embedded.addresses[i].street;
                document.getElementById('woonplaats').value = data._embedded.addresses[i].city.label;
                document.getElementById('provincie').value = data._embedded.addresses[i].province.label;
            }
            console.log("Er is data gevonden.")
        },
    })
}
$(document).ready(function() {
    $('#huisnummer').change(function() {
        getAdress(); //haal de data op en update de data in de vakjes

        console.log("Data ingevuld.");
    });
});
